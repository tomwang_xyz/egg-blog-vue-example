import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const router = new VueRouter({
	mode: 'hash',
	routes: [
		{
			path: '/',
			component: ()=>import('../views/Home')
		},
		{
			path: '/detail',
			component: ()=>import('../views/Detail')
		},
		{
			path: '/add',
			component: ()=>import('../views/Add')
		}
	]
})

export default router
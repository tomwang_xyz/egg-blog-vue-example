import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { 
	Button,
	Tabbar,
	TabbarItem,
	Uploader,
	Cell,
	CellGroup,
	Field,
	List,
	Toast
} from 'vant'

Vue.use(Button)
Vue.use(Tabbar)
Vue.use(TabbarItem)
Vue.use(Uploader)
Vue.use(Cell)
Vue.use(CellGroup)
Vue.use(Field)
Vue.use(List)
Vue.use(Toast)

Vue.config.productionTip = false

new Vue({
	router,
  render: h => h(App),
}).$mount('#app')

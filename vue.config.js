module.exports = {
	devServer: {
		proxy: {
			'/article': {
				target: 'http://127.0.0.1:7020',
				ws: true,
				changeOrgin: true
			}
		}
	}
}